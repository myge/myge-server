import { Client } from 'colyseus';
import { CampaignRoom } from '../Rooms/CampaignRoom';

import { getRoomNameTest } from './CampaignRoom/getRoomNameTest';
import { deleteRoom } from './CampaignRoom/deleteRoom';
import { rollDice } from './CampaignRoom/rollDice';

import { addNewArea } from './CampaignRoom/addNewArea';
import { deleteArea } from './CampaignRoom/deleteArea';
import { modifyArea } from './CampaignRoom/modifyArea';
import { setPlayerArea } from './CampaignRoom/setPlayerArea';

import { setWallTile } from './CampaignRoom/setWallTile';

import { addNewCharacter } from './CampaignRoom/addNewCharacter';
import { modifyCharacter } from './CampaignRoom/modifyCharacter';
import { setCharacterOwner } from './CampaignRoom/setCharacterOwner';
import { setCharacterPortrait } from './CampaignRoom/setCharacterPortrait';

import { addNewToken } from './CampaignRoom/addNewToken';
import { deleteToken } from './CampaignRoom/deleteToken';
import { moveToken } from './CampaignRoom/moveToken';
import { setTokenVisibility } from './CampaignRoom/setTokenVisibility';

import { downloadImage } from './CampaignRoom/downloadImage';
import { getImagesFileList } from './CampaignRoom/getImagesFileList';

const services: { [key: string]: (room: CampaignRoom, client: Client, params: any, root?: boolean) => any } = {
	getRoomNameTest,
	rollDice,
	addNewArea,
	deleteArea,
	modifyArea,
	setPlayerArea,
	addNewCharacter,
	addNewToken,
	deleteToken,
	moveToken,
	setTokenVisibility,
	modifyCharacter,
	setWallTile,
	setCharacterOwner,
	setCharacterPortrait,
	getImagesFileList,
	deleteRoom,
	downloadImage
};

export { services };
