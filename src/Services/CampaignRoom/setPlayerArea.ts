import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';

import { AreaState } from '../../Schemas/States/AreaState';

export function setPlayerArea (room: CampaignRoom, client: Client, params: any, root?: boolean) : object {
	if (!root && (client || {}).sessionId != room.state.hostSessionId) {
		throw new Error('Only the GM is allowed to set the current players area');
	}

	const areaId = (params || {}).areaId;

	if (typeof(areaId) === 'undefined') {
		throw new Error('The ID of the area to delete (params.areaId) not given');
	}

	// Find the area of the given ID to make sure it exists
	const area = room.state.areas.get(areaId);

	if (typeof(area) === 'undefined') {
		throw new Error('No area of ID ' + areaId);
	}

	// Finally set it as the "player" area
	room.state.playerAreaId = areaId;

	return {};
}
