import fs from 'fs';
import path from 'path';

import { v4 as uuidv4 } from 'uuid';
import request from 'request';

import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';

export function downloadImage (room: CampaignRoom, client: Client, params: any, root?: boolean) : object {
	const href = params.externalImageURL;
	const url = new URL(href);

	const parts = url.pathname.split('.');
	const extension = parts[parts.length - 1];

	const imagesUploadedDir = path.join('files', 'images', 'uploaded');
	const filename = uuidv4();
	const fullFilename = filename + '.' + extension;

	const filepath = path.join(imagesUploadedDir, fullFilename);

	const downloadPromise = new Promise(function (resolve, reject) {
		request.get(url.href, function (error, response, body) {
			if (error) {
				reject(new Error(error));
			} else {
				const imageURI = 'images/uploaded/' + fullFilename;
				resolve({ imageURI });
			}
		}).pipe(fs.createWriteStream(filepath));
	});

	return downloadPromise;
}
