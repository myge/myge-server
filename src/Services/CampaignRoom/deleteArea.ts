import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';

import { AreaState } from '../../Schemas/States/AreaState';

export function deleteArea (room: CampaignRoom, client: Client, params: any, root?: boolean) : object {
	if (!root && (client || {}).sessionId != room.state.hostSessionId) {
		throw new Error('Only the GM is allowed to delete areas');
	}

	const areaId = (params || {}).areaId;

	if (typeof(areaId) === 'undefined') {
		throw new Error('The ID of the area to delete (params.areaId) not given');
	}

	// Find the index for area of the given ID
	const areaIdx = room.state.orderedAreas.findIndex((_id: string) => _id === areaId);

	if (areaIdx === -1) {
		throw new Error('No area of ID ' + areaId);
	}

	// Delete the area from the state
	room.state.orderedAreas.splice(areaIdx, 1);
	room.state.areas.delete(areaId);

	// If it was the players area, set the players area to nothing
	if (room.state.playerAreaId === areaId) {
		room.state.playerAreaId = '';
	}

	return {};
}
