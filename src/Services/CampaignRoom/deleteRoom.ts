import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';

export function deleteRoom (room: CampaignRoom, client: Client, params: any, root?: boolean) : object {
	if (!root && (client || {}).sessionId != room.state.hostSessionId) {
		throw new Error('Only the GM is allowed to delete the room');
	}

	// Ka-boom
	room.delete();

	return {};
}
