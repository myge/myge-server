import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';

export function setWallTile (room: CampaignRoom, client: Client, params: any, root?: boolean) : object {
	const sessionId = (client || {}).sessionId;

	// Check permissions
	if (!root && sessionId !== room.state.hostSessionId) {
		throw new Error('Only the GM is allowed to edit walls');
	}

	// Find area
	const areaId = (params || {}).areaId;
	if (!areaId) {
		throw new Error('The ID of area with target tile not given (params.areaId)');
	}
	const area = room.getAreaById(areaId);
	if (!area) {
		throw new Error('No area of ID ' + areaId);
	}

	// Validate position
	const posX = (params.position || {}).x;
	const posY = (params.position || {}).y;
	if (typeof(posX) === 'undefined' || typeof(posY) === 'undefined') {
		throw new Error('Incomplete position given (either x, y or both missing)');
	}
	if (posX < 0 || posX >= area.areaSettings.gridWidth || posY < 0 || posY >= area.areaSettings.gridHeight) {
		throw new Error(`Position (${posX}, ${posY}) out of bounds (area "${area.name}" is ${area.areaSettings.gridWidth}-by-${area.areaSettings.gridHeight})`);
	}

	// Set wall tile status in the area
	const posString = Math.floor(posX).toString() + "x" + Math.floor(posY).toString();
	const wall = Boolean((params || {}).wall);

	if (wall) {
		area.wallTiles.set(posString, true);
	} else {
		area.wallTiles.delete(posString);
	}

	return {};
}
