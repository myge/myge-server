import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';

import { isPositionWithinBounds, posStringIntToPosition } from '../../Utils/AreaUtils';

import { AreaState } from '../../Schemas/States/AreaState';
import { TokenState } from '../../Schemas/States/TokenState';

import { areaSettings as deserializeAreaSettings } from '../../Utils/Deserializer';

export async function modifyArea (room: CampaignRoom, client: Client, params: any, root?: boolean) : Promise<object> {
	if (!root && (client || {}).sessionId != room.state.hostSessionId) {
		throw new Error('Only the GM is allowed to modify area specification');
	}

	const areaId = (params || {}).areaId;
	const areaChangesJSON = (params || {}).areaChangesJSON || '{}';

	if (typeof(areaId) === 'undefined') {
		throw new Error('The ID of the area to modify (params.areaId) not given');
	}

	// Find the area of the given ID
	const area = room.state.areas.get(areaId);

	if (typeof(area) === 'undefined') {
		throw new Error('No area of ID ' + areaId);
	}

	const areaChangesObj = JSON.parse(areaChangesJSON);
	if (Object.keys(areaChangesObj).length < 1) {
		throw new Error('No changes to apply (params.areaChanges)');
	}

	// Apply changes to the existing object
	area.name = areaChangesObj.name;
	area.areaSettings = deserializeAreaSettings(areaChangesObj.areaSettings);

	// Delete all stuff that is out of area bounds
	const cleanupPromises: Array<Promise<any>> = [];
	area.tokens.forEach(function (token: TokenState, tokenId: string) {
		if (!isPositionWithinBounds(area, token.pos.x, token.pos.y)) {
			// If a token is out of *new* area bounds, create a Promise for a token
			//  deletion service call. Those promises must be awaited later outside
			//  the forEach() loop.
			cleanupPromises.push(room.callServiceLocal("deleteToken", {
				areaId: areaId,
				tokenId: tokenId
			}));
		}
	});
	area.wallTiles.forEach(function (wall: boolean, posStringInt: string) {
		const pos = posStringIntToPosition(posStringInt);
		if (!isPositionWithinBounds(area, pos.x, pos.y)) {
			// No promise needed here, this is a trivial action
			area.wallTiles.delete(posStringInt);
		}
	});
	await Promise.all(cleanupPromises);

	return {};
}
