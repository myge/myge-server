import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';

export function deleteToken (room: CampaignRoom, client: Client, params: any, root?: boolean) : object {
	const sessionId = (client || {}).sessionId;

	// Check permissions
	if (!root && sessionId !== room.state.hostSessionId) {
		throw new Error('Only the GM is allowed to delete tokens');
	}

	// Find area
	const areaId = (params || {}).areaId;
	if (!areaId) {
		throw new Error('The ID of area holding target token not given (params.tokenId)');
	}
	const area = room.getAreaById(areaId);
	if (!area) {
		throw new Error('No area of ID ' + areaId);
	}

	// Find token in the area
	const tokenId = (params || {}).tokenId;
	if (!tokenId) {
		throw new Error('The ID of token to move not given (params.tokenId)');
	}
	const token = area.tokens.get(tokenId);
	if (!token) {
		throw new Error('No token of ID ' + tokenId + ' in area "' + area.name + '"');
	}

	// Get character
	const character = room.state.characters.get(token.characterId);
	if (!character) {
		throw new Error('No character found of ID ' + token.characterId);
	}

	// Remove token-character link
	character.representedByTokens.delete(tokenId);

	// Automatically remove characters not linked to any tokens
	// TODO: Allow characters to exist independently of tokens in the future
	if (character.representedByTokens.size === 0) {
		room.state.characters.delete(character.id);
	}

	// Delete token from the state
	area.tokens.delete(tokenId);

	return {};
}
