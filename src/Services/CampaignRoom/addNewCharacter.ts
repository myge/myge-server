import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';
import { v4 as uuidv4 } from 'uuid';

import { CharacterState } from '../../Schemas/States/CharacterState';

export function addNewCharacter (room: CampaignRoom, client: Client, params: any, root?: boolean) : object {
	if (!root && (client || {}).sessionId != room.state.hostSessionId) {
		throw new Error('Only the GM is allowed to create characters');
	}

	const characterId = uuidv4();
	const ownerSessionId = params.ownerSessionId || '';
	const representingTokenId = params.representingTokenId;
	const representingTokenAreaId = params.representingTokenAreaId;

	if (representingTokenId && !representingTokenAreaId) {
		throw new Error('Token ID was given, but corresponding area ID was not');
	}

	const character = new CharacterState().assign({
		id: characterId,
		ownerSessionId: ownerSessionId
	});

	if (representingTokenId) {
		character.representedByTokens.set(representingTokenId, representingTokenAreaId);
	}

	// Add character to the state
	room.state.characters.set(characterId, character);

	return { character, characterId };
}
