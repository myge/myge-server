import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';
import { v4 as uuidv4 } from 'uuid';

import { Position } from '../../Schemas/States/Position';
import { TokenState } from '../../Schemas/States/TokenState';

export async function addNewToken (room: CampaignRoom, client: Client, params: any, root?: boolean) : Promise<object> {
	if (!root && (client || {}).sessionId != room.state.hostSessionId) {
		throw new Error('Only the GM is allowed to create tokens');
	}

	// Check if a correct area id is given
	const areaId = params.areaId;
	if (!areaId) {
		throw new Error('No areaId given (a token must always exist in a specific area)');
	}
	const area = room.getAreaById(areaId);
	if (!area) {
		throw new Error('No area of ID ' + areaId);
	}

	// Check if a correct position is given
	const posX = (params.position || {}).x;
	const posY = (params.position || {}).y;

	if (typeof(posX) === 'undefined' || typeof(posY) === 'undefined') {
		throw new Error('Incomplete position given (either x, y or both missing)');
	}

	if (posX < 0 || posX >= area.areaSettings.gridWidth || posY < 0 || posY >= area.areaSettings.gridHeight) {
		throw new Error(`Position (${posX}, ${posY}) out of bounds (area "${area.name}" is ${area.areaSettings.gridWidth}-by-${area.areaSettings.gridHeight})`);
	}

	// Generate token id
	const tokenId = uuidv4();

	// Check if character ID is given. If not, create a whole new character
	let characterId = params.characterId;
	if (!characterId) {
		const characterCreationResult = await room.callServiceLocal("addNewCharacter", {
			representingTokenId: tokenId,
			representingTokenAreaId: areaId
		});
		characterId = characterCreationResult.characterId;
	}

	const position = new Position().assign({ x: posX, y: posY });
	const visible = params.visible || false;

	// Create the token object
	const token = new TokenState().assign({
		id: tokenId,
		pos: position,
		visibleToGmOnly: !visible,
		characterId: characterId
	});

	// Add the token to the area state
	area.tokens.set(tokenId, token);

	return { token, tokenId };
}
