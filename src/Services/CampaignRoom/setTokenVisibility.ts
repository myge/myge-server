import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';

export function setTokenVisibility (room: CampaignRoom, client: Client, params: any, root?: boolean) : object {
	const sessionId = (client || {}).sessionId;

	// Check permissions
	if (!root && sessionId !== room.state.hostSessionId) {
		throw new Error('Only the GM is allowed to hide/unhide tokens');
	}

	// Find area
	const areaId = (params || {}).areaId;
	if (!areaId) {
		throw new Error('The ID of area holding target token not given (params.tokenId)');
	}
	const area = room.getAreaById(areaId);
	if (!area) {
		throw new Error('No area of ID ' + areaId);
	}

	// Find token in the area
	const tokenId = (params || {}).tokenId;
	if (!tokenId) {
		throw new Error('The ID of token to move not given (params.tokenId)');
	}
	const token = area.tokens.get(tokenId);
	if (!token) {
		throw new Error('No token of ID ' + tokenId + ' in area "' + area.name + '"');
	}

	// Set token's visibility
	const visible = Boolean((params || {}).visible);
	token.visibleToGmOnly = !visible;

	return {};
}
