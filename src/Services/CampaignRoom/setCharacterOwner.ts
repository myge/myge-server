import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';

export function setCharacterOwner (room: CampaignRoom, client: Client, params: any, root?: boolean) : object {
	// Check permissions
	const clientSessionId = (client || {}).sessionId;
	if (!root && clientSessionId !== room.state.hostSessionId) {
		throw new Error('Only the GM is allowed to set the character\'s owner');
	}

	// Find the character of the given ID
	const characterId = (params || {}).characterId;
	const character = room.state.characters.get(characterId);
	if (typeof(character) === 'undefined') {
		throw new Error('No character of ID ' + characterId);
	}

	// Validate new owner's session ID
	const ownerSessionId = (params || {}).ownerSessionId;
	if (ownerSessionId !== '' && !(room.isClientSessionIdValid(ownerSessionId))) {
		throw new Error('No such player with sessionId: ' + ownerSessionId);
	}

	// Apply changes
	character.ownerSessionId = ownerSessionId;

	return {};
}
