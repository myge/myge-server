import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';

import { AttributeValue, BoolFlagValue, BoolTrackValue, ResourceValue, TextEnumValue, TextValue } from '../../Schemas/Config/RulesetFieldValues';

export function modifyCharacter (room: CampaignRoom, client: Client, params: any, root?: boolean) : object {
	const characterId = (params || {}).characterId;

	// Find the character of the given ID
	const character = room.state.characters.get(characterId);
	if (typeof(character) === 'undefined') {
		throw new Error('No character of ID ' + characterId);
	}

	// Check permissions
	const clientSessionId = (client || {}).sessionId;
	if (!root && clientSessionId !== room.state.hostSessionId && clientSessionId !== character.ownerSessionId) {
		throw new Error('Only the GM and the character\'s owner is allowed to modify the character');
	}

	// Apply changes
	const fieldChanges = (params || {}).fieldChanges || [];
	for (const fieldChange of fieldChanges) {
		const fieldName = fieldChange.fieldName;
		if (!fieldName) {
			throw new Error('No field name given');
		}

		const type = fieldChange.type;
		if (type === 'Attribute') {
			let value = character.attributeValues.get(fieldName);
			if (!value) {
				value = new AttributeValue().assign({ fieldName });
				character.attributeValues.set(fieldName, value);
			}
			if ('genericToggle' in fieldChange) value.genericToggle = fieldChange.genericToggle;
			if ('value' in fieldChange) value.value = fieldChange.value;
			if ('tempModifier' in fieldChange) value.tempModifier = fieldChange.tempModifier;
			if ('tempValueOverride' in fieldChange) value.tempValueOverride = fieldChange.tempValueOverride;
		}
		else if (type === 'Bool') {
			let value = character.boolFlagValues.get(fieldName);
			if (!value) {
				value = new BoolFlagValue().assign({ fieldName });
				character.boolFlagValues.set(fieldName, value);
			}
			if ('value' in fieldChange) value.value = fieldChange.value;
		}
		else if (type === 'BoolTrack') {
			let value = character.boolTrackValues.get(fieldName);
			if (!value) {
				value = new BoolTrackValue().assign({ fieldName });
				character.boolTrackValues.set(fieldName, value);
			}
			if ('genericToggle' in fieldChange) value.genericToggle = fieldChange.genericToggle;
			if ('toggles' in fieldChange) {
				const toggles = fieldChange.toggles;
				for (let i = 0; i < toggles.length; i++) {
					if (i < value.toggles.length) {
						value.toggles[i] = toggles[i];
					} else {
						value.toggles.push(toggles[i]);
					}
				}
			}
		}
		else if (type === 'Resource') {
			let value = character.resourceValues.get(fieldName);
			if (!value) {
				value = new ResourceValue().assign({ fieldName });
				character.resourceValues.set(fieldName, value);
			}
			if ('genericToggle' in fieldChange) value.genericToggle = fieldChange.genericToggle;
			if ('current' in fieldChange) value.current = fieldChange.current;
			if ('max' in fieldChange) value.max = fieldChange.max;
			if ('tempCurrentModifier' in fieldChange) value.tempCurrentModifier = fieldChange.tempCurrentModifier;
			if ('tempMaxModifier' in fieldChange) value.tempMaxModifier = fieldChange.tempMaxModifier;
			if ('tempBuffer' in fieldChange) value.tempBuffer = fieldChange.tempBuffer;
		}
		else if (type === 'TextEnum') {
			let value = character.textEnumValues.get(fieldName);
			if (!value) {
				value = new TextEnumValue().assign({ fieldName });
				character.textEnumValues.set(fieldName, value);
			}
			if ('genericToggle' in fieldChange) value.genericToggle = fieldChange.genericToggle;
			if ('optionIdx' in fieldChange) value.optionIdx = fieldChange.optionIdx;
		}
		else if (type === 'Text') {
			let value = character.textValues.get(fieldName);
			if (!value) {
				value = new TextValue().assign({ fieldName });
				character.textValues.set(fieldName, value);
			}
			if ('genericToggle' in fieldChange) value.genericToggle = fieldChange.genericToggle;
			if ('text' in fieldChange) value.text = fieldChange.text;
		}
		else {
			throw new Error('Unrecognized field type: "' + type + '"');
		}
	}

	return {};
}
