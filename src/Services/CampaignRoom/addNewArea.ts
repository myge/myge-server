import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';
import { v4 as uuidv4 } from 'uuid';

import { AreaSettings } from '../../Schemas/Config/AreaSettings';
import { AreaState } from '../../Schemas/States/AreaState';

export function addNewArea (room: CampaignRoom, client: Client, params: any, root?: boolean) : object {
	if (!root && (client || {}).sessionId != room.state.hostSessionId) {
		throw new Error('Only the GM is allowed to create areas');
	}

	const areaName = (params || {}).name || 'Unnamed area';
	const areaId = uuidv4();

	// Take the setting defaults assigned to the campaign
	const areaSettings = new AreaSettings().assign(room.state.areaSettingsDefault);

	const area = new AreaState().assign({
		id: areaId,
		name: areaName,
		areaSettings: areaSettings
	});

	// Add the area to the state
	room.state.orderedAreas.push(areaId);
	room.state.areas.set(areaId, area);

	return { area, areaId };
}
