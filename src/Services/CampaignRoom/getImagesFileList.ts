import fs from 'fs';
import path from 'path';

import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';

export async function getImagesFileList (room: CampaignRoom, client: Client, params: any, root?: boolean) : Promise<object> {
	const imagesUploadedDir = path.join('files', 'images', 'uploaded');
	const imagesUploadedFileNames = await fs.promises.readdir(imagesUploadedDir);
	const imagesUploadedURIs = imagesUploadedFileNames.map(function (_fileName: string) {
		return 'images/uploaded/' + _fileName;
	});

	return imagesUploadedURIs;
}
