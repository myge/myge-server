import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';
import randomNumber from 'random-number-csprng';

export async function rollDice (room: CampaignRoom, client: Client, params: any, root?: boolean) : Promise<object> {
	const broadcast = typeof(params.broadcast) !== 'undefined' ? params.broadcast : true;
	const rolls = params.rolls || [];
	const modifier = params.modifier || 0;

	const individualResults = [];
	let finalResult = 0;

	for (let roll of rolls) {
		// The roll is {times}d{sides}
		const sides = roll.sides;
		if (!sides) {
			throw new Error('Number of sides of a die not given');
		}
		const times = roll.times || 1;
		const sign = Math.sign(times);

		// Roll each die separately
		for (let i = 0; i < Math.abs(times); i++) {
			let result = sign * await randomNumber(1, sides);
			individualResults.push({ sides, result });
			finalResult += result;
		}
	}

	// Add the modifier once, at the end
	finalResult += modifier;

	// Optionally broadcast the result of the roll
	if (broadcast) {
		// Always send either JSONs or flat (no collections) schemas as messages.
		//  Complex objects in messages are very problematic for Colyseus, they
		//  mostly work with states.
		room.broadcast('diceRollResult', JSON.stringify({
			rollerSessionId: (client || {}).sessionId || 'Server',
			rolls,
			individualResults,
			modifier,
			finalResult
		}));
	}

	return { individualResults, modifier, finalResult };
}
