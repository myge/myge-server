import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';

export function moveToken (room: CampaignRoom, client: Client, params: any, root?: boolean) : object {
	const sessionId = (client || {}).sessionId;

	// Find area
	const areaId = (params || {}).areaId;
	if (!areaId) {
		throw new Error('The ID of area holding target token not given (params.tokenId)');
	}
	const area = room.getAreaById(areaId);
	if (!area) {
		throw new Error('No area of ID ' + areaId);
	}

	// Validate position
	const posX = (params.position || {}).x;
	const posY = (params.position || {}).y;
	if (typeof(posX) === 'undefined' || typeof(posY) === 'undefined') {
		throw new Error('Incomplete position given (either x, y or both missing)');
	}
	if (posX < 0 || posX >= area.areaSettings.gridWidth || posY < 0 || posY >= area.areaSettings.gridHeight) {
		throw new Error(`Position (${posX}, ${posY}) out of bounds (area "${area.name}" is ${area.areaSettings.gridWidth}-by-${area.areaSettings.gridHeight})`);
	}

	// Find token in the area
	const tokenId = (params || {}).tokenId;
	if (!tokenId) {
		throw new Error('The ID of token to move not given (params.tokenId)');
	}
	const token = area.tokens.get(tokenId);
	if (!token) {
		throw new Error('No token of ID ' + tokenId + ' in area "' + area.name + '"');
	}

	// Hidden tokens may only be moved by the GM
	if (token.visibleToGmOnly && !root && sessionId !== room.state.hostSessionId) {
		throw new Error('Hidden tokens may only be moved by the GM');
	}

	// It's possible that the target position is the same as the current one, in
	//  which case - do nothing
	if (Math.abs(token.pos.x - posX) < 0.001 && Math.abs(token.pos.y - posY) < 0.001) {
		return {};
	}

	// Get character
	const character = room.state.characters.get(token.characterId);
	if (!character) {
		throw new Error('No character found of ID ' + token.characterId);
	}

	// Check permissions
	if (!root && sessionId !== room.state.hostSessionId && sessionId !== character.ownerSessionId) {
		throw new Error('Only the GM and the character\'s owner (if any) can move tokens representing that character');
	}

	// Move the token
	token.pos.x = posX;
	token.pos.y = posY;

	return {};
}
