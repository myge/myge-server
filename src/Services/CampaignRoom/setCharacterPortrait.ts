import fs from 'fs';
import path from 'path';

import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';

export async function setCharacterPortrait (room: CampaignRoom, client: Client, params: any, root?: boolean) : Promise<object> {
	// Check permissions
	const clientSessionId = (client || {}).sessionId;
	if (!root && clientSessionId !== room.state.hostSessionId) {
		throw new Error('Only the GM is allowed to set a character\'s portrait');
	}

	// Find the character of the given ID
	const characterId = (params || {}).characterId;
	const character = room.state.characters.get(characterId);
	if (typeof(character) === 'undefined') {
		throw new Error('No character of ID ' + characterId);
	}

	// Validate image URI
	const imageURI = (params || {}).imageURI;
	if (imageURI !== '') {
		const fullPath = path.join('files', imageURI);
		// The line below will throw if the file doesn't exist
		await fs.promises.access(fullPath, fs.constants.F_OK);
	}

	// Apply changes
	character.portrait.imageURI = imageURI;
	character.portrait.tokenOffset.x = ((params || {}).tokenOffset || {}).x || character.portrait.tokenOffset.x;
	character.portrait.tokenOffset.y = ((params || {}).tokenOffset || {}).y || character.portrait.tokenOffset.y;
	character.portrait.tokenScale = (params || {}).tokenScale || character.portrait.tokenScale;

	return {};
}
