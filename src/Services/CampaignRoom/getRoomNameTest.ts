import { Room, Client } from 'colyseus';
import { CampaignRoom } from '../../Rooms/CampaignRoom';

// A trivial test service that returns the room's visible name and throws if
//  requested to do so.
export function getRoomNameTest (room: CampaignRoom, client: Client, params: any, root?: boolean) : object {
	if (params.error) {
		throw new Error(params.error);
	}

	return {
		visibleName: room.state.visibleName
	};
}
