import http from "http";
import express from "express";
import fileUpload from "express-fileUpload";
import { UploadedFile } from "express-fileUpload";
import cors from "cors";
import path from "path";
import fs from "fs";
import mime from "mime-types";

import { Server } from "colyseus";
import { monitor } from "@colyseus/monitor";
import { v4 as uuidv4 } from 'uuid';
// import socialRoutes from "@colyseus/social/express"

import { CampaignRoom } from "./Rooms/CampaignRoom";

const dirPathsToCreate = [
	path.join('files', 'images', 'uploaded'),
	path.join('saves')
];
for (const path of dirPathsToCreate) {
	fs.exists(path, function (exists: boolean) {
		if (!exists) {
			fs.mkdir(path, { recursive: true }, function (err, path) {
				if (err) {
					console.error('Error creating directory', path, ':', err);
				}
			});
		}
	});
}

const port = Number(process.env.PORT || 2567);
const app = express()

app.use(fileUpload({
	createParentPath: true
}));
app.use(cors());
app.use(express.json())

const server = http.createServer(app);
const gameServer = new Server({
	server,
});

// register your room handlers
gameServer.define('campaign', CampaignRoom);

app.post('/upload-images', async function (req, res) {
	try {
		if (!req.files) {
			res.send({
				status: false,
				message: 'No files uploaded'
			});
		} else {
			console.log(req.files);

			const fileDetails = [];

			let uploadedFiles: any = req.files.images;
			if (!Array.isArray(uploadedFiles)) {
				uploadedFiles = [ uploadedFiles ];
			}

			const proxyHost: any = req.headers['x-forwarded-host'];
			const host = proxyHost ? proxyHost : req.headers.host;

			const proxyProto: any = req.headers['x-forwarded-proto'];
			const proto = proxyProto ? proxyProto : req.protocol;

			const fullHost = proto + '://' + host;

			for (const key in uploadedFiles) {
				const file: fileUpload.UploadedFile = uploadedFiles[key];

				const name = uuidv4();
				const extension = mime.extension(file.mimetype);
				file.name = name + '.' + extension;

				const filePath = path.join('files', 'images', 'uploaded', file.name);
				file.mv(filePath);

				const fileURLPath = 'images/uploaded/' + file.name;
				const fileURL = new URL(fileURLPath, fullHost);

				fileDetails.push({
					name: file.name,
					mimetype: file.mimetype,
					size: file.size,
					href: fileURL.href,
					uri: fileURLPath
				});

				console.log('File uploaded:', file);
			}

			res.send({
				status: true,
				message: 'Files uploaded successfully',
				files: fileDetails
			});
		}
	} catch (err) {
		console.error('Error uploading file:', err);
		res.status(500).send(err);
	}
});

app.use(express.static('files'));

/**
 * Register @colyseus/social routes
 *
 * - uncomment if you want to use default authentication (https://docs.colyseus.io/server/authentication/)
 * - also uncomment the import statement
 */
// app.use("/", socialRoutes);

// register colyseus monitor AFTER registering your room handlers
app.use("/colyseus", monitor());

gameServer.listen(port);
console.log(`Listening on ws://localhost:${ port }`)
