import { AreaSettings } from '../Schemas/Config/AreaSettings';
import { RPGSettings } from '../Schemas/Config/RPGSettings';
import { Ruleset } from '../Schemas/Config/Ruleset';
import { AttributeField, BoolFlagField, BoolTrackField, ResourceField, TextEnumField, TextField } from '../Schemas/Config/RulesetFields';
import { AttributeValue, BoolFlagValue, BoolTrackValue, ResourceValue, TextEnumValue, TextValue } from '../Schemas/Config/RulesetFieldValues';

import { AreaState } from '../Schemas/States/AreaState';
import { CampaignState } from '../Schemas/States/CampaignState';
import { CharacterState } from '../Schemas/States/CharacterState';
import { PortraitState } from '../Schemas/States/PortraitState';
import { Position } from '../Schemas/States/Position';
import { TokenState } from '../Schemas/States/TokenState';

function deserializeArraySchema(obj: any, array: any, itemDeserializer: (item: any) => any) {
	const items = (obj || {}).Items || obj || [];
	for (const i in items) {
		const item = items[i];
		array.push(itemDeserializer(item));
	}
}

function deserializeMapSchema(obj: any, map: any, itemDeserializer: (item: any) => any) {
	const items = (obj || {}).Items || obj || {};
	for (const key in items) {
		const item = items[key];
		map.set(key, itemDeserializer(item));
	}
}

function areaSettings(obj: any) : AreaSettings {
	const res = new AreaSettings().assign({
		distanceCalculationMode: obj.distanceCalculationMode,
		gridWidth: obj.gridWidth,
		gridHeight: obj.gridHeight,
		distanceUnit: obj.distanceUnit,
		gridSquareSideLength: obj.gridSquareSideLength
	});
	deserializeArraySchema(obj.definedFields, res.definedFields, x => x);
	return res;
}

function attributeField(obj: any) : AttributeField {
	return new AttributeField().assign(obj);
}

function boolFlagField(obj: any) : BoolFlagField {
	return new BoolFlagField().assign(obj);
}

function boolTrackField(obj: any) : BoolTrackField {
	const field = new BoolTrackField().assign({
		name: obj.name,
		category: obj.category,
		showGenericToggle: obj.showGenericToggle,
		gmOnly: obj.gmOnly,
		visualizationMode: obj.visualizationMode,
	});
	deserializeArraySchema(obj.toggleLabels, field.toggleLabels, x => x);
	return field;
}

function resourceField(obj: any) : ResourceField {
	return new ResourceField().assign(obj);
}

function textEnumField(obj: any) : TextEnumField {
	const field = new TextEnumField().assign({
		name: obj.name,
		category: obj.category,
		showGenericToggle: obj.showGenericToggle,
		gmOnly: obj.gmOnly,
		visualizationMode: obj.visualizationMode,
		textColorHex: obj.textColorHex
	});
	deserializeArraySchema(obj.options, field.options, x => x);
	return field;
}

function textField(obj: any) : TextField {
	return new TextField().assign(obj);
}

function attributeValue(obj: any) : AttributeValue {
	return new AttributeValue().assign(obj);
}

function boolFlagValue(obj: any) : BoolFlagValue {
	return new BoolFlagValue().assign(obj);
}

function boolTrackValue(obj: any) : BoolTrackValue {
	const res = new BoolTrackValue().assign({
		fieldName: obj.fieldName,
		genericToggle: obj.genericToggle
	});
	deserializeArraySchema(obj.toggles, res.toggles, x => x);
	return res;
}

function resourceValue(obj: any) : ResourceValue {
	return new ResourceValue().assign(obj);
}

function textEnumValue(obj: any) : TextEnumValue {
	return new TextEnumValue().assign(obj);
}

function textValue(obj: any) : TextValue {
	return new TextValue().assign(obj);
}

function ruleset(obj: any) : Ruleset {
	const res = new Ruleset().assign({
		name: obj.name
	});

	deserializeArraySchema(obj.orderedFields, res.orderedFields, x => x);
	deserializeMapSchema(obj.fieldTypeNames, res.fieldTypeNames, x => x);

	deserializeMapSchema(obj.attributeFields, res.attributeFields, attributeField);
	deserializeMapSchema(obj.boolFlagFields, res.boolFlagFields, boolFlagField);
	deserializeMapSchema(obj.boolTrackFields, res.boolTrackFields, boolTrackField);
	deserializeMapSchema(obj.resourceFields, res.resourceFields, resourceField);
	deserializeMapSchema(obj.textEnumFields, res.textEnumFields, textEnumField);
	deserializeMapSchema(obj.textFields, res.textFields, textField);

	return res;
}

function rpgSettings(obj: any) : RPGSettings {
	const res = new RPGSettings();
	res.ruleset = ruleset(obj.ruleset || {});
	deserializeArraySchema(obj.definedFields, res.definedFields, x => x);
	return res;
}

function areaState(obj: any) : AreaState {
	const res = new AreaState().assign({
		id: obj.id,
		name: obj.name
	});
	res.areaSettings = areaSettings(obj.areaSettings || {});
	deserializeMapSchema(obj.tokens, res.tokens, tokenState);
	deserializeMapSchema(obj.wallTiles, res.wallTiles, x => x);
	return res;
}

function campaignState(obj: any) : CampaignState {
	const res = new CampaignState().assign({
		hostSessionId: obj.hostSessionId,
		visibleName: obj.visibleName,
		playerAreaId: obj.playerAreaId
	});
	deserializeMapSchema(obj.allPlayersSessionIDs, res.allPlayersSessionIDs, x => x);
	res.rpgSettings = rpgSettings(obj.rpgSettings || {});
	res.areaSettingsDefault = areaSettings(obj.areaSettingsDefault || {});
	deserializeArraySchema(obj.orderedAreas, res.orderedAreas, x => x);
	deserializeMapSchema(obj.areas, res.areas, areaState);
	deserializeMapSchema(obj.characters, res.characters, characterState);
	return res;
}

function characterState(obj: any) : CharacterState {
	const res = new CharacterState().assign({
		id: obj.id,
		ownerSessionId: obj.ownerSessionId
	});

	deserializeMapSchema(obj.representedByTokens, res.representedByTokens, x => x);

	deserializeMapSchema(obj.attributeValues, res.attributeValues, attributeValue);
	deserializeMapSchema(obj.boolFlagValues, res.boolFlagValues, boolFlagValue);
	deserializeMapSchema(obj.boolTrackValues, res.boolTrackValues, boolTrackValue);
	deserializeMapSchema(obj.resourceValues, res.resourceValues, resourceValue);
	deserializeMapSchema(obj.textEnumValues, res.textEnumValues, textEnumValue);
	deserializeMapSchema(obj.textValues, res.textValues, textValue);

	res.portrait = portraitState(obj.portrait || {});

	return res;
}

function portraitState(obj: any) : PortraitState {
	const res = new PortraitState().assign({
		imageURI: obj.imageURI,
		tokenScale: obj.tokenScale
	});
	res.tokenOffset = position(obj.tokenOffset || {});
	return res;
}

function position(obj: any) : Position {
	return new Position().assign(obj);
}

function tokenState(obj: any) : TokenState {
	const res = new TokenState().assign({
		id: obj.id,
		visibleToGmOnly: obj.visibleToGmOnly,
		characterId: obj.characterId
	});
	res.pos = position(obj.pos || {});
	return res;
}

export {
	deserializeArraySchema,
	areaSettings,
	attributeField,
	boolFlagField,
	boolTrackField,
	resourceField,
	textEnumField,
	textField,
	ruleset,
	rpgSettings,
	attributeValue,
	boolFlagValue,
	boolTrackValue,
	resourceValue,
	textEnumValue,
	textValue,
	areaState,
	campaignState,
	characterState,
	portraitState,
	position,
	tokenState
};
