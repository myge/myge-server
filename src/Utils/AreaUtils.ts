import { AreaState } from '../Schemas/States/AreaState';
import { Position } from '../Schemas/States/Position';

function isPositionWithinBounds (area: AreaState, x: number, y: number) : boolean {
	const outOfBounds = (x < 0 || x >= area.areaSettings.gridWidth || y < 0 || y >= area.areaSettings.gridHeight);
	return !outOfBounds;
}

function posStringIntToPosition (posStringInt: string) : Position {
	const values = posStringInt.split('x').map((value) => parseInt(value, 10));

	if (values.length !== 2) {
		throw new Error('"' + posStringInt + '" is not a valid position string; 2 values expected, ' + values.length + ' found');
	}

	const pos = new Position().assign({
		x: values[0],
		y: values[1]
	});

	return pos;
}

export {
	isPositionWithinBounds,
	posStringIntToPosition
};
