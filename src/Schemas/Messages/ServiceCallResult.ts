import { Schema, type } from '@colyseus/schema';

export class ServiceCallResult extends Schema {
	@type('string')
	callId: string;

	@type('string')
	serviceName: string;

	@type('string')
	error: string;

	@type('string')
	resultJSON: string;
}
