import { Schema, type } from '@colyseus/schema';

export class ServiceCall extends Schema {
	@type('string')
	callId: string;

	@type('string')
	serviceName: string;

	@type('string')
	paramsJSON: string;
}
