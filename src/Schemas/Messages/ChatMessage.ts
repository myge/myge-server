import { Schema, type } from '@colyseus/schema';

export class ChatMessage extends Schema {
	@type('string')
	authorSessionId: string;

	@type('string')
	message: string;
}
