import { MapSchema, Schema, type } from '@colyseus/schema';

import { AreaSettings } from '../Config/AreaSettings';
import { TokenState } from './TokenState';

export class AreaState extends Schema {
	@type('string')
	id: string;

	@type('string')
	name: string;

	@type( AreaSettings )
	areaSettings: AreaSettings = new AreaSettings();

	@type({ map: TokenState })
	tokens = new MapSchema<TokenState>();

	@type({ map: 'boolean' })
	wallTiles = new MapSchema<boolean>();
}
