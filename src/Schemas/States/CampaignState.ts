import { ArraySchema, MapSchema, Schema, type } from "@colyseus/schema";

import { AreaSettings } from '../Config/AreaSettings';
import { RPGSettings } from '../Config/RPGSettings';

import { AreaState } from './AreaState';
import { CharacterState } from './CharacterState';

export class CampaignState extends Schema {
	@type('string')
	hostSessionId: string;

	@type({ map: 'boolean' })
	allPlayersSessionIDs = new MapSchema<boolean>();

	@type('string')
	visibleName: string;

	@type( RPGSettings )
	rpgSettings: RPGSettings = new RPGSettings();

	@type( AreaSettings )
	areaSettingsDefault: AreaSettings = new AreaSettings();

	@type(['string'])
	orderedAreas = new ArraySchema<string>();

	@type({ map: AreaState })
	areas = new MapSchema<AreaState>();

	@type({ map: CharacterState })
	characters = new MapSchema<CharacterState>();

	@type('string')
	playerAreaId: string = '';
}
