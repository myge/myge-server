import { MapSchema, Schema, type } from '@colyseus/schema';

import { AttributeValue } from '../Config/RulesetFieldValues/AttributeValue';
import { BoolFlagValue } from '../Config/RulesetFieldValues/BoolFlagValue';
import { BoolTrackValue } from '../Config/RulesetFieldValues/BoolTrackValue';
import { ResourceValue } from '../Config/RulesetFieldValues/ResourceValue';
import { TextEnumValue } from '../Config/RulesetFieldValues/TextEnumValue';
import { TextValue } from '../Config/RulesetFieldValues/TextValue';

import { PortraitState } from './PortraitState';

export class CharacterState extends Schema {
	@type('string')
	id: string;

	@type('string')
	ownerSessionId: string = '';

	// Map containing area IDs, keyed by token IDs
	@type({ map: 'string' })
	representedByTokens = new MapSchema<string>();

	@type({ map: AttributeValue })
	attributeValues = new MapSchema<AttributeValue>();

	@type({ map: BoolFlagValue })
	boolFlagValues = new MapSchema<BoolFlagValue>();

	@type({ map: BoolTrackValue })
	boolTrackValues = new MapSchema<BoolTrackValue>();

	@type({ map: ResourceValue })
	resourceValues = new MapSchema<ResourceValue>();

	@type({ map: TextEnumValue })
	textEnumValues = new MapSchema<TextEnumValue>();

	@type({ map: TextValue })
	textValues = new MapSchema<TextValue>();

	@type( PortraitState )
	portrait = new PortraitState();
}
