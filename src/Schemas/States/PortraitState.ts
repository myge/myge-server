import { MapSchema, Schema, type } from '@colyseus/schema';

import { Position } from './Position';

export class PortraitState extends Schema {
	@type('string')
	imageURI: string;

	@type( Position )
	tokenOffset = new Position();

	@type('float32')
	tokenScale: number;
}
