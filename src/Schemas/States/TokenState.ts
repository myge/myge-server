import { Schema, type } from '@colyseus/schema';

import { Position } from './Position';

export class TokenState extends Schema {
	@type('string')
	id: string;

	@type( Position )
	pos: Position = new Position();

	@type('boolean')
	visibleToGmOnly: boolean = true;

	@type('string')
	characterId: string;
}
