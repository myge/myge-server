import { type } from '@colyseus/schema';

import { SettingsGroup } from './SettingsGroup';

export class AreaSettings extends SettingsGroup {
	@type('int32')
	distanceCalculationMode: number = 0;

	@type('int32')
	gridWidth: number = 10;

	@type('int32')
	gridHeight: number = 10;

	@type('string')
	distanceUnit: string = 'ft';

	@type('float64')
	gridSquareSideLength: number = 5.0;
}
