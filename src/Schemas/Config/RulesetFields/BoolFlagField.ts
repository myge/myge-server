import { type } from '@colyseus/schema';
import { RulesetField } from './RulesetField';

export class BoolFlagField extends RulesetField {
	@type('string')
	symbolColorHex: string = "cyan";
}
