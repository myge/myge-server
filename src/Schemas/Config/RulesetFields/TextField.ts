import { type } from '@colyseus/schema';
import { RulesetField } from './RulesetField';

export class TextField extends RulesetField {
	@type('string')
	textColorHex: string = "white";
}
