import { Schema, type } from '@colyseus/schema';

export abstract class RulesetField extends Schema {
	@type('string')
	name: string;

	@type('string')
	category: string;

	@type('boolean')
	showGenericToggle: boolean = false;

	@type('boolean')
	gmOnly: boolean = false;

	@type('int32')
	visualizationMode: number = 0;
}
