import { type } from '@colyseus/schema';
import { RulesetField } from './RulesetField';

export class AttributeField extends RulesetField {
	@type('string')
	textColorHex: string = "white";

	@type('string')
	rollCommandArgument: string;
}
