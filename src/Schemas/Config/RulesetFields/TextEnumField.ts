import { ArraySchema, type } from '@colyseus/schema';
import { RulesetField } from './RulesetField';

export class TextEnumField extends RulesetField {
	@type(['string'])
	options = new ArraySchema<string>();

	@type('string')
	textColorHex: string = "white";
}
