import { type } from '@colyseus/schema';
import { RulesetField } from './RulesetField';

export class ResourceField extends RulesetField {
	@type('string')
	barColorHex: string = "green";
}
