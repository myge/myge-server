import { ArraySchema, type } from '@colyseus/schema';
import { RulesetField } from './RulesetField';

export class BoolTrackField extends RulesetField {
	@type(['string'])
	toggleLabels = new ArraySchema<string>();
}
