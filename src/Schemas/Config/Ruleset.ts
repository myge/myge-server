import { ArraySchema, MapSchema, Schema, type } from '@colyseus/schema';

import { AttributeField } from './RulesetFields/AttributeField';
import { BoolFlagField } from './RulesetFields/BoolFlagField';
import { BoolTrackField } from './RulesetFields/BoolTrackField';
import { ResourceField } from './RulesetFields/ResourceField';
import { TextEnumField } from './RulesetFields/TextEnumField';
import { TextField } from './RulesetFields/TextField';

export class Ruleset extends Schema {
	@type('string')
	name: string;

	@type(['string'])
	orderedFields = new ArraySchema<string>();

	@type({ map: 'string' })
	fieldTypeNames = new MapSchema<string>();

	@type({ map: AttributeField })
	attributeFields = new MapSchema<AttributeField>();

	@type({ map: BoolFlagField })
	boolFlagFields = new MapSchema<BoolFlagField>();

	@type({ map: BoolTrackField })
	boolTrackFields = new MapSchema<BoolTrackField>();

	@type({ map: ResourceField })
	resourceFields = new MapSchema<ResourceField>();

	@type({ map: TextEnumField })
	textEnumFields = new MapSchema<TextEnumField>();

	@type({ map: TextField })
	textFields = new MapSchema<TextField>();
}
