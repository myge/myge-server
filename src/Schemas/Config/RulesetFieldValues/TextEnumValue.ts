import { type } from '@colyseus/schema';
import { RulesetFieldValue } from './RulesetFieldValue';

export class TextEnumValue extends RulesetFieldValue {
	@type('int32')
	optionIdx: number;
}
