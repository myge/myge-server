import { ArraySchema, type } from '@colyseus/schema';
import { RulesetFieldValue } from './RulesetFieldValue';

export class BoolTrackValue extends RulesetFieldValue {
	@type(['boolean'])
	toggles = new ArraySchema<boolean>();
}
