import { type } from '@colyseus/schema';
import { RulesetFieldValue } from './RulesetFieldValue';

export class TextValue extends RulesetFieldValue {
	@type('string')
	text: string;
}
