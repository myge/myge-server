import { type } from '@colyseus/schema';
import { RulesetFieldValue } from './RulesetFieldValue';

export class AttributeValue extends RulesetFieldValue {
	@type('int32')
	value: number;

	@type('int32')
	tempModifier: number;

	@type('int32')
	tempValueOverride: number;
}
