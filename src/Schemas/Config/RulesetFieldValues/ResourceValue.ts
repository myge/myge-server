import { type } from '@colyseus/schema';
import { RulesetFieldValue } from './RulesetFieldValue';

export class ResourceValue extends RulesetFieldValue {
	@type('int32')
	current: number;

	@type('int32')
	max: number;

	@type('int32')
	tempCurrentModifier: number;

	@type('int32')
	tempMaxModifier: number;

	@type('int32')
	tempBuffer: number;
}
