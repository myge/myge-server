import { Schema, type } from '@colyseus/schema';

export abstract class RulesetFieldValue extends Schema {
	@type('string')
	fieldName: string;

	@type('boolean')
	genericToggle: boolean = false;
}
