import { type } from '@colyseus/schema';
import { RulesetFieldValue } from './RulesetFieldValue';

export class BoolFlagValue extends RulesetFieldValue {
	@type('boolean')
	value: boolean = false;
}
