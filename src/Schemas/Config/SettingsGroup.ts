import { ArraySchema, Schema, type } from '@colyseus/schema';

export abstract class SettingsGroup extends Schema {
	@type(['string'])
	definedFields = new ArraySchema<string>();
}
