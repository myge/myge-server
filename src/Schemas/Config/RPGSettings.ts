import { type } from '@colyseus/schema';

import { SettingsGroup } from './SettingsGroup';

import { Ruleset } from './Ruleset';

export class RPGSettings extends SettingsGroup {
	@type( Ruleset )
	ruleset: Ruleset = new Ruleset();
}
