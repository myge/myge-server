import fs from 'fs';
import path from 'path';
import cron from 'node-cron';

import { Room, Client } from 'colyseus';

import { AreaState } from '../Schemas/States/AreaState';
import { CampaignState } from '../Schemas/States/CampaignState';
import { ChatMessage } from '../Schemas/Messages/ChatMessage';
import { ServiceCall } from '../Schemas/Messages/ServiceCall';
import { ServiceCallResult } from '../Schemas/Messages/ServiceCallResult';

import { areaSettings, rpgSettings, campaignState } from '../Utils/Deserializer';

import { services } from '../Services/CampaignRoomServiceList';

export class CampaignRoom extends Room {
	autosaveTask: cron.ScheduledTask;

	applyOptions (options: any) {
		// Generate and save an identifier for the room
		const visibleName = (options.metadata || {}).visibleName || 'Room #' + this.roomId;
		this.setMetadata({ visibleName });
		this.state.visibleName = visibleName;

		// Propagate provided config defaults (if any)
		const rpgSettingsObj = JSON.parse(options.rpgSettingsJSON || '{}');
		const areaSettingsDefaultObj = JSON.parse(options.areaSettingsDefaultJSON || '{}');

		this.state.rpgSettings = rpgSettings(rpgSettingsObj);
		this.state.areaSettingsDefault = areaSettings(areaSettingsDefaultObj);
	}

	async callService (client: Client, serviceName: string, callId: string, params: any) : Promise<ServiceCallResult> {
		const serviceFunc = services[serviceName];

		const message = new ServiceCallResult();
		message.callId = callId;
		message.serviceName = serviceName;

		try {
			// All of this is wrapped inside of a try-catch because serviceFunc()
			//  may also throw errors.
			if (serviceFunc) {
				const result = await serviceFunc(this, client, params);
				message.resultJSON = JSON.stringify(result);
			} else {
				throw new Error('(' + callId + ' from ' + client.sessionId + ') No such service found: ' + name);
			}
		} catch (error) {
			message.error = error.toString();
		} finally {
			return message;
		}
	}

	async callServiceLocal (serviceName: string, params: any, parentServiceCaller: Client = null, parentCallId: string = null) : Promise<any> {
		const serviceFunc = services[serviceName];

		const parentCallInfo = [];
		if (parentCallId) {
			parentCallInfo.push(`parent call ${parentCallId}`);
		}
		if (parentServiceCaller) {
			parentCallInfo.push(`from ${parentServiceCaller.sessionId}`);
		}
		const parentCallInfoMsg = parentCallInfo.length > 0 ? `, ${parentCallInfo.join(' ')}` : '';

		this.log('Local service call' + parentCallInfoMsg + ':', {
			serviceName: serviceName,
			params: JSON.stringify(params)
		});

		try {
			if (serviceFunc) {
				const result = await serviceFunc(this, parentServiceCaller, params, true);
				this.log('Local service call executed' + parentCallInfoMsg + ':', {
					result: JSON.stringify(result)
				});
				return result;
			} else {
				throw new Error('(Local service call' + parentCallInfoMsg + ') No such service found: ' + name);
			}
		} catch (error) {
			throw new Error('(Local service call' + parentCallInfoMsg + ') ' + error.toString());
		}
	}

	delete () {
		const self = this;
		self.broadcast('roomDeleted');
		self.autoDispose = true;
		self.disconnect();
	}

	getAreaById(areaId: string) : AreaState {
		return this.state.areas.get(areaId);
	}

	getSaveFilePath() : string {
		const filename = this.roomId + '.json';
		const filepath = path.join('saves', filename);
		return filepath;
	}

	isClientSessionIdValid(sessionId: string) : boolean {
		return this.state.allPlayersSessionIDs.has(sessionId);
	}

	async loadState() : Promise<boolean> {
		const self = this;

		const filepath = self.getSaveFilePath();
		try {
			const buffer = await fs.promises.readFile(filepath);

			const stateJSON = buffer.toString();
			const stateObj = JSON.parse(stateJSON);

			self.setMetadata({ visibleName: stateObj.visibleName });
			const state = campaignState(stateObj);

			self.setState(campaignState(stateObj));

			self.log('Original JSON:', stateJSON);
			self.log('State var:', JSON.stringify(state));
			self.log('State successfully loaded from ' + filepath + ':', JSON.stringify(self.state));
			return true;
		}
		catch (error) {
			self.log('Cannot load saved state from ' + filepath + '.', error);
			return false;
		}
	}

	registerFileUploadListeners() {
		const self = this;

		const uploadedImagesPath = path.join('files', 'images', 'uploaded');
		fs.watch(uploadedImagesPath, function (event: string, filename: string) {
			const filePath = path.join(uploadedImagesPath, filename);
			if (event === 'rename') {
				fs.exists(filePath, function (exists: boolean) {
					if (exists) {
						// File uploaded
					} else {
						// File deleted
					}
				});
			} else if (event === 'change') {
				// File changed (this fires on file creation too)
			}
		});
	}

	registerMessageHandlers () {
		const self = this;

		// Simple chat messages relay
		self.onMessage('chat', function (client, message) {
			self.log('[Chat] ' + client.sessionId + ': ' + message);
			const chatMsg = new ChatMessage();
			chatMsg.authorSessionId = client.sessionId;
			chatMsg.message = message;
			self.broadcast('chat', chatMsg);
		});

		// Service call incoming message
		self.onMessage('callService', async function (client, message) {
			self.log('Service call from ' + client.sessionId + ':', {
				callId: message.callId,
				serviceName: message.serviceName,
				paramsJSON: message.paramsJSON
			});
			const params = JSON.parse((message || {}).paramsJSON || '{}');
			const resultMessage = await self.callService(client, message.serviceName, message.callId, params);
			self.log('Service call executed:', {
				callId: resultMessage.callId,
				serviceName: resultMessage.serviceName,
				error: resultMessage.error,
				resultJSON: resultMessage.resultJSON
			});
			client.send('callServiceResult', resultMessage);
		});

		// Fallback message handler
		self.onMessage('*', function (client, type, message) {
			self.log('Msg "' + type + '" from ' + client.sessionId + ':' + message);
		});
	}

	initializeState () {
		this.setState(new CampaignState());

		// Start with empty host ID, to later recognize if it has been set yet
		this.state.hostSessionId = null;
	}

	async saveState () : Promise<void> {
		const self = this;

		const stateJSON = JSON.stringify(self.state);
		const filepath = self.getSaveFilePath();

		try {
			await fs.promises.writeFile(filepath, stateJSON);
			self.log('Room state saved successfully');
		} catch (error) {
			self.logWarning('Error saving room state:', error);
		}
	}

	// Room event handlers
	async onCreate (options: any) {
		const self = this;

		// If the room ID is given by the client, use that instead
		if (options.id) {
			this.roomId = options.id;
			this.logDebug('Room being re-created with options:', options);
		} else {
			this.logDebug('New room being created with options:', options);
		}

		// Try to reload a previously saved state, if not initialize a new one
		const stateLoaded = await this.loadState();
		if (!stateLoaded) {
			// Initialize an empty state before applying any options - applyOptions
			//  may modify the state.
			this.initializeState();
			this.applyOptions(options);
		}
		this.registerMessageHandlers();
		this.registerFileUploadListeners();

		// Don't auto-dispose
		this.autoDispose = false;

		// Schedule autosave
		this.autosaveTask = cron.schedule('* * * * *', function () {
			self.saveState();
		});

		this.log('New room succesfully created');
	}

	async onJoin (client: Client, options: any) {
		if (options.sessionId) {
			// If a different session ID is given in the options, that means the client
			//  is trying to restore an old session.
			// The reason we're not using the Colyseus reconnection for this, is that
			//  it won't work if the server has been restarted.
			client.sessionId = options.sessionId;
			if (this.state.hostSessionId == client.sessionId) {
				this.log(client.sessionId + ' is the host');
			} else {
				this.log('Client ' + client.sessionId + ' has joined');
			}
		} else {
			if (!this.state.hostSessionId) {
				// If the host ID hasn't been set, it means the person joining is the
				//  creator of the room, therefore they are the host. This is done only
				//  once for each individual room.
				this.state.hostSessionId = client.sessionId;
				this.log(client.sessionId + ' is the host');
			} else {
				// Otherwise, it's just a regular client joining.
				this.log('Client ' + client.sessionId + ' has joined');
			}

			// Keep track of all sessionIDs of all players that have ever joined.
			// This is useful for ID validation when a player happens not to be
			//  connected at the time.
			this.state.allPlayersSessionIDs.set(client.sessionId, true);
		}
	}

	async onLeave (client: Client, consented: boolean) {
		if (consented) {
			this.log('Client ' + client.sessionId + ' has left');
		} else {
			this.log('Client ' + client.sessionId + ' lost connection');
		}

		// Always leave reconnection open
		try {
			await this.allowReconnection(client);
			this.log('Client ' + client.sessionId + ' has reconnected');
		} catch (error) {
			// The time has expired. This should never happen.
			this.logWarning('Client ' + client.sessionId + ' somehow timed out - the room was likely deleted.');
		}
	}

	async onDispose() {
		this.log('Room disposed of');

		if (this.autosaveTask) this.autosaveTask.stop();

		// Don't bother saving the state if the room was set to auto-dispose
		if (!(this.autoDispose)) {
			await this.saveState();
		}
	}

	// Logging functions
	log (...args: any[]) {
		console.log('[CampaignRoom] ' + this.roomId + ':', ...args);
	}

	logDebug (...args: any[]) {
		this.log(...args);
	}

	logWarning (...args: any[]) {
		console.warn('[CampaignRoom] ' + this.roomId + ':', ...args);
	}
}
